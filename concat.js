const fs = require("fs");
const execa = require("execa");
const { _: argumentFiles } = require("minimist")(process.argv.slice(2));

async function concatClips(files) {
  const inputListFile = "inputList.txt";
  const contents = files.map(file => `file ${file}\n`).join("");
  fs.writeFileSync(inputListFile, contents);
  const output = "joined.mp4";
  const command = `ffmpeg -f concat -i ${inputListFile} -c copy ${output}`;
  await execa.shellSync(command, {
    stdio: [process.stdin, process.stdout, process.stderr]
  });
  fs.unlinkSync(inputListFile);
  return output;
}

(async function() {
  console.log("Input files:", argumentFiles);
  const output = await concatClips(argumentFiles);
  console.log("Concatenated to",output);
})();
