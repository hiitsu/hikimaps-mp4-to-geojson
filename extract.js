const fs = require("fs");
const execa = require("execa");
const replaceExt = require("replace-ext");
const { _: argumentFiles } = require("minimist")(process.argv.slice(2));

async function extractTelemetryToBinary(input) {
  const output = replaceExt(input, ".bin");
  const command = `ffmpeg -y -i ${input} -codec copy -map 0:3:m:handler_name:"GoPro MET" -f rawvideo ${output}`;
  await execa.shellSync(command, {
    stdio: [process.stdin, process.stdout, process.stderr]
  });
  return output;
}

async function binaryToJson(input) {
  const output = replaceExt(input, ".json2");
  const command = `go run bin/gopro2json/gopro2json.go -i ${input} -o ${output}`;
  await execa.shellSync(command, {
    stdio: [process.stdin, process.stdout, process.stderr]
  });
  return output;
}

(async function() {
  console.log("Input files:", argumentFiles);
  const telemetryBinaryFiles = await Promise.all(
    argumentFiles.map(file => extractTelemetryToBinary(file))
  );
  const jsonFiles = await Promise.all(
    telemetryBinaryFiles.map(file => binaryToJson(file))
  );
  const telemetry = jsonFiles
    .map(file => {
      return JSON.parse(fs.readFileSync(file));
    })
    .reduce((acc, json) => {
      console.log("Concating count:", json.data.length);
      return acc.concat(json.data);
    }, [])
    .map((o, index, array) => {
      const startUtc = array[0].utc;
      const offsetMillis = (o.utc - startUtc) / 1000;
      return [o.lat, o.lon, offsetMillis, o.alt, o.spd_3d];
    });
  const outputFile = "telemetry.json";
  fs.writeFileSync(outputFile, JSON.stringify(telemetry));
  console.log("Telemetry extracted to", outputFile);
})();
